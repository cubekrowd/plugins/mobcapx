package net.cubekrowd.mobcapx;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public final class ChunkTaskQueue {
    private final MobCapXPlugin plugin;
    private final Queue<ChunkTask> queue = new LinkedList<>();

    public ChunkTaskQueue(MobCapXPlugin plugin) {
        this.plugin = plugin;
    }

    public MobCapXPlugin getPlugin() {
        return plugin;
    }

    public void tick() {
        int desiredOperations = plugin.getChunkOperationsPerTick();
        int operations = 0;
        // Note that tasks can add other tasks to the queue. These shouldn't be
        // executed in the same tick, so we need to keep track of the current
        // size and only run tasks up until then.
        int queueSize = queue.size();

        for (int i = 0; i < queueSize; i++) {
            var task = queue.remove();
            operations += task.run(this);

            if (operations >= desiredOperations) {
                break;
            }
        }
    }

    public void add(ChunkTask newTask) {
        queue.offer(newTask);
    }

    public List<ChunkTask> getTasks() {
        return List.copyOf(queue);
    }
}
