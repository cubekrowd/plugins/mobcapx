package net.cubekrowd.mobcapx;

import java.util.UUID;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;

public final class UpdatePlayerTask implements ChunkTask {
    private UUID playerId;
    // @TODO support world unloading (also a problem in other classes)
    private World lastKnownWorld;
    private SquareChunkRegion lastKnownArea;
    private String lastKnownPlayerName;

    public UpdatePlayerTask(Player player) {
        playerId = player.getUniqueId();
        lastKnownPlayerName = player.getName();
    }

    @Override
    public String getName() {
        return "Player update - " + lastKnownPlayerName;
    }

    private int removeFromOldArea(MobCapXPlugin plugin) {
        // remove player from chunks in the old area
        var chunkMap = plugin.getChunkMap();
        int updates = 0;

        for (var chunkId : lastKnownArea) {
            var chunkData = chunkMap.getChunkData(lastKnownWorld, chunkId);
            if (chunkData == null) {
                continue;
            }

            chunkData.removeNearbyPlayer(playerId);
            updates++;
        }

        return updates;
    }

    @Override
    public int run(ChunkTaskQueue queue) {
        var plugin = queue.getPlugin();
        var player = plugin.getServer().getPlayer(playerId);

        if (player == null) {
            // player is offline, so remove player data
            plugin.removePlayerData(playerId);

            if (lastKnownWorld != null) {
                return removeFromOldArea(plugin);
            }
            return 1;
        }

        // update player name in case they relogged
        lastKnownPlayerName = player.getName();

        var currentWorld = player.getWorld();
        var currentChunk = player.getChunk();

        // Note: currently we update even if the player is still in the same
        // chunk, because there may be new chunks loaded around the player. If
        // we don't update, this means their limit will stay low.

        // @TODO could probably be optimised a bit by only updating the
        // non-overlapping parts of the old area and new area

        int res = 0;

        if (lastKnownWorld != null) {
            // remove from chunks in the old area
            res += removeFromOldArea(plugin);
        }

        var playerData = plugin.getPlayerData(player.getUniqueId());

        if (player.getGameMode() == GameMode.SPECTATOR) {
            // Spectators shouldn't be counted as nearby players, otherwise
            // areas with lots of spectators will get more spawns. Additionally,
            // spectators could force spawning where there should be none.
            lastKnownWorld = null;
            lastKnownArea = null;
            // this sets the player's area limits
            playerData.setNearbyChunkCount(plugin, 0);

            queue.add(this);
            return res + 2;
        }

        // now add player to the chunks in the new area
        var newArea = new SquareChunkRegion(currentChunk, plugin.getNearbyRange());
        int nearbyChunkCount = 0;
        var chunkMap = plugin.getChunkMap();

        // Also recompute player's area limits and values!
        playerData.clearAreaValues();

        for (var chunkId : newArea) {
            var chunkData = chunkMap.getChunkData(currentWorld, chunkId);
            if (chunkData == null) {
                continue;
            }

            chunkData.addNearbyPlayer(player);
            float weight = chunkData.getWeight();
            playerData.incrementAreaValues(chunkData.getCategoryCounts(), weight);
            nearbyChunkCount++;
        }

        // this sets the player's area limits
        playerData.setNearbyChunkCount(plugin, nearbyChunkCount);

        res += nearbyChunkCount;

        lastKnownWorld = currentWorld;
        lastKnownArea = newArea;

        queue.add(this);
        return res;
    }
}
