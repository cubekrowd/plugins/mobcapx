package net.cubekrowd.mobcapx;

import java.util.Iterator;
import java.util.NoSuchElementException;
import org.bukkit.Chunk;

public final class SquareChunkRegion implements ChunkRegion {
    private final int minX;
    private final int minZ;
    // Number fo chunks in the x and z direction. Always at least 1.
    private final int width;

    public SquareChunkRegion(int centreX, int centreZ, int range) {
        this.minX = centreX - range;
        this.minZ = centreZ - range;
        width = 2 * range + 1;
    }

    public SquareChunkRegion(Chunk centre, int range) {
        this(centre.getX(), centre.getZ(), range);
    }

    @Override
    public Iterator<ChunkId> iterator() {
        return new SquareRegionIterator();
    }

    private final class SquareRegionIterator implements Iterator<ChunkId> {
        private int nextX = minX;
        private int nextZ = minZ;
        private final int endX = minX + width;
        private final int maxZ = minZ + width - 1;
        private final ChunkId mutableChunkId = ChunkId.mutableOf(0, 0);

        // We first move along the z-axis, then do one step along the x-axis,
        // then all along the z-axis, etc.

        @Override
        public boolean hasNext() {
            return nextX < endX;
        }

        @Override
        public ChunkId next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            // first store result values
            mutableChunkId.setXZ(nextX, nextZ);

            // now update nextX and nextZ to their next positions
            if (nextZ == maxZ) {
                nextX++;
                nextZ = minZ;
            } else {
                nextZ++;
            }

            return mutableChunkId;
        }
    }
}
