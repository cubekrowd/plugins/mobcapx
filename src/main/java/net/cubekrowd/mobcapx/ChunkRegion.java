package net.cubekrowd.mobcapx;

// A collection of chunks (or really chunk IDs, so without a world). Mainly
// useful for iterating over the chunk IDs it references.
public interface ChunkRegion extends Iterable<ChunkId> {
}
