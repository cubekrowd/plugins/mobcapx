package net.cubekrowd.mobcapx;

import java.util.Arrays;

public final class PlayerData {
    // the player's area values are ALWAYS the sums of the total weights of the
    // chunks in the player's nearby area
    private final float[] areaValues = new float[MobCategory.ENUM_COUNT - 1];
    private final float[] areaLimits = new float[MobCategory.ENUM_COUNT - 1];
    private boolean online = true;

    public void clearAreaValues() {
        Arrays.fill(areaValues, 0);
    }

    public void setNearbyChunkCount(MobCapXPlugin plugin, int newNearbyChunkCount) {
        // compute area limits for all mob categories
        for (var category : MobCategory.values()) {
            if (category == MobCategory.MISC) {
                continue;
            }

            float areaLimit = plugin.computeLimit(category, newNearbyChunkCount);
            areaLimits[category.ordinal()] = areaLimit;
        }
    }

    public float getAreaValue(MobCategory category) {
        return areaValues[category.ordinal()];
    }

    public void incrementAreaValue(MobCategory category, float add) {
        areaValues[category.ordinal()] += add;
    }

    public void incrementAreaValues(float[] counts, float weight) {
        for (int i = 0; i < areaValues.length; i++) {
            areaValues[i] += counts[i] * weight;
        }
    }

    public float getAreaLimit(MobCategory category) {
        return areaLimits[category.ordinal()];
    }

    public void setOnline(boolean newOnline) {
        online = newOnline;
    }

    public boolean isOnline() {
        return online;
    }
}
