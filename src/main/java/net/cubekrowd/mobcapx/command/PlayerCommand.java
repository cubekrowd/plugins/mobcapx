package net.cubekrowd.mobcapx.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import net.cubekrowd.mobcapx.MobCapXPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class PlayerCommand implements MobCapXCommand.Subcommand {
    private final MobCapXPlugin plugin;
    private final List<Subcommand> subcommands;

    public interface Subcommand {
        String getName();

        String getPermission();

        default boolean hasPermission(CommandSender sender) {
            var perm = getPermission();
            return perm == null || sender.hasPermission(perm);
        }

        String getUsage();

        void execute(CommandSender sender, Player target, String[] args, Message.Builder m);

        List<String> complete(CommandSender sender, String[] args);
    }

    public PlayerCommand(MobCapXPlugin plugin) {
        this.plugin = plugin;
        subcommands = List.of(
                new PlayerAreaCommand(plugin),
                new ChunksCommand(plugin));
    }

    @Override
    public String getName() {
        return "player";
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public String getUsage() {
        return "<player> ...";
    }

    private void sendHelp(CommandSender sender, Message.Builder m) {
        var msg = m.info().text("Accessible player subcommands:");
        boolean empty = true;

        for (var sub : subcommands) {
            if (sub.hasPermission(sender)) {
                empty = false;
                msg.bullet().highlight(sub.getName() + " " + sub.getUsage());
            }
        }

        if (empty) {
            msg.text(" none.");
        }

        msg.send(sender);
    }

    @Override
    public void execute(CommandSender sender, String[] args, Message.Builder m) {
        if (args.length >= 2) {
            var target = plugin.getServer().getPlayer(args[0]);

            if (target == null) {
                m.warn().text("Player ").highlight(args[0]).text(" not found.").send(sender);
                return;
            }

            var lowerArg1 = args[1].toLowerCase(Locale.ENGLISH);

            for (var sub : subcommands) {
                if (lowerArg1.equals(sub.getName())) {
                    if (!sub.hasPermission(sender)) {
                        // send no subcommand found message
                        break;
                    }

                    sub.execute(sender, target, Arrays.copyOfRange(args, 2, args.length), m);
                    return;
                }
            }

            // no subcommand found
        }

        sendHelp(sender, m);
    }

    @Override
    public List<String> complete(CommandSender sender, String[] args) {
        if (args.length == 1) {
            var lowerArg0 = args[0].toLowerCase(Locale.ENGLISH);
            var onlinePlayers = plugin.getServer().getOnlinePlayers();
            var res = new ArrayList<String>(onlinePlayers.size());

            for (var onlinePlayer : onlinePlayers) {
                if (onlinePlayer.getName().toLowerCase(Locale.ENGLISH).startsWith(lowerArg0)) {
                    res.add(onlinePlayer.getName());
                }
            }
            return res;
        } else if (args.length == 2) {
            var lowerArg1 = args[1].toLowerCase(Locale.ENGLISH);
            var res = new ArrayList<String>(subcommands.size());

            for (var sub : subcommands) {
                if (sub.getName().startsWith(lowerArg1) && sub.hasPermission(sender)) {
                    res.add(sub.getName());
                }
            }
            return res;
        } else {
            // args.length > 1, delegate to subcommand
            var lowerArg1 = args[1].toLowerCase(Locale.ENGLISH);

            for (var sub : subcommands) {
                if (sub.getName().equals(lowerArg1)) {
                    if (!sub.hasPermission(sender)) {
                        // treat as subcommand not found
                        break;
                    }

                    return sub.complete(sender, Arrays.copyOfRange(args, 2, args.length));
                }
            }

            // no subcommand found
            return Collections.emptyList();
        }
    }
}
