package net.cubekrowd.mobcapx.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import net.cubekrowd.mobcapx.MobCapXPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public final class MobCapXCommand implements CommandExecutor, TabCompleter {
    private final MobCapXPlugin plugin;
    private final Message.Builder m;
    private final List<Subcommand> subcommands;

    public interface Subcommand {
        String getName();

        String getPermission();

        default boolean hasPermission(CommandSender sender) {
            var perm = getPermission();
            return perm == null || sender.hasPermission(perm);
        }

        String getUsage();

        void execute(CommandSender sender, String[] args, Message.Builder m);

        List<String> complete(CommandSender sender, String[] args);
    }

    public MobCapXCommand(MobCapXPlugin plugin) {
        this.plugin = plugin;
        m = new Message.Builder(plugin);
        subcommands = List.of(
                new AboutCommand(plugin),
                new ReloadCommand(plugin),
                new WorldCommand(plugin),
                new PlayerCommand(plugin),
                new QueueCommand(plugin),
                new HelpCommand(plugin));
    }

    public List<Subcommand> getSubcommands() {
        return subcommands;
    }

    private void displayDefaultMessage(CommandSender sender) {
        var version = plugin.getDescription().getVersion();
        var authors = plugin.getDescription().getAuthors();

        var msg = m.info().text("Running version ").highlight(version).text(" by ");

        for (int i = 0; i < authors.size(); i++) {
            msg.highlight(authors.get(i));

            if (i < authors.size() - 2) {
                msg.text(", ");
            } else if (i == authors.size() - 2) {
                msg.text(" and ");
            } else {
                msg.text(".");
            }
        }

        msg.text(" Enter ").highlight("/mobcapx help").text(" for help.");
        msg.send(sender);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command,
            String label, String[] args) {
        // Permission is registered in plugin.yml so there's no need for the
        // command permission.
        if (args.length == 0) {
            displayDefaultMessage(sender);
            return true;
        } else {
            var lowerArg0 = args[0].toLowerCase(Locale.ENGLISH);

            for (var sub : subcommands) {
                if (lowerArg0.equals(sub.getName())) {
                    if (!sub.hasPermission(sender)) {
                        // send no subcommand found message
                        break;
                    }

                    sub.execute(sender, Arrays.copyOfRange(args, 1, args.length), m);
                    return true;
                }
            }

            // no subcommand found
            displayDefaultMessage(sender);
            return true;
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command,
            String alias, String[] args) {
        // note that args.length is always larger than 0
        if (args.length == 1) {
            var lowerArg0 = args[0].toLowerCase(Locale.ENGLISH);
            var res = new ArrayList<String>(subcommands.size());

            for (var sub : subcommands) {
                if (sub.getName().startsWith(lowerArg0) && sub.hasPermission(sender)) {
                    res.add(sub.getName());
                }
            }
            return res;
        } else {
            // args.length > 1, delegate to subcommand
            var lowerArg0 = args[0].toLowerCase(Locale.ENGLISH);

            for (var sub : subcommands) {
                if (sub.getName().equals(lowerArg0)) {
                    if (!sub.hasPermission(sender)) {
                        // treat as subcommand not found
                        break;
                    }

                    return sub.complete(sender, Arrays.copyOfRange(args, 1, args.length));
                }
            }

            // no subcommand found
            return Collections.emptyList();
        }
    }
}
