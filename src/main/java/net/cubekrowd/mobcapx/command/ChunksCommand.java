package net.cubekrowd.mobcapx.command;

import java.util.Collections;
import java.util.List;
import net.cubekrowd.mobcapx.MobCapXPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class ChunksCommand implements PlayerCommand.Subcommand {
    private final MobCapXPlugin plugin;

    public ChunksCommand(MobCapXPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getName() {
        return "chunks";
    }

    @Override
    public String getPermission() {
        return "mobcapx.debug";
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public void execute(CommandSender sender, Player target, String[] args, Message.Builder m) {
        var msg = m.info().text("Nearby chunks for ").highlight(target.getName()).text(":");
        int serverTick = plugin.getServer().getCurrentTick();

        for (var world : plugin.getServer().getWorlds()) {
            for (var loadedChunk : world.getLoadedChunks()) {
                var chunkData = plugin.getChunkMap().getChunkData(loadedChunk);
                if (!chunkData.isNearby(target)) {
                    continue;
                }

                int x = loadedChunk.getX();
                int z = loadedChunk.getZ();

                float[] categoryCounts = chunkData.getCategoryCounts();
                int totalCount = 0;
                for (float count : categoryCounts) {
                    totalCount += count;
                }

                if (totalCount == 0) {
                    continue;
                }

                msg.bullet().text("(" + x + ", " + z + "):");
                msg.highlight(" Mo:" + (int) categoryCounts[0]);
                msg.highlight(" An:" + (int) categoryCounts[1]);
                msg.highlight(" Wa:" + (int) categoryCounts[2]);
                msg.highlight(" Am:" + (int) categoryCounts[3]);

                int recalcTick = chunkData.getRecalculateCategoryCountTick();
                if (recalcTick == -1) {
                    msg.highlight(" Age:inf");
                } else {
                    msg.highlight(" Age:" + (serverTick - recalcTick));
                }
            }
        }

        msg.send(sender);
    }

    @Override
    public List<String> complete(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}
