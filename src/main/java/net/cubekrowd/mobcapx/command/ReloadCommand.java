package net.cubekrowd.mobcapx.command;

import java.util.Collections;
import java.util.List;
import net.cubekrowd.mobcapx.MobCapXPlugin;
import org.bukkit.command.CommandSender;

public final class ReloadCommand implements MobCapXCommand.Subcommand {
    private final MobCapXPlugin plugin;

    public ReloadCommand(MobCapXPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getName() {
        return "reload";
    }

    @Override
    public String getPermission() {
        return "mobcapx.reload";
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public void execute(CommandSender sender, String[] args, Message.Builder m) {
        // Try to save config, so users can regenerate a deleted config. This is
        // useful for updating to a newer config version.
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        m.info().text("Reloaded the configuration file!").send(sender);
    }

    @Override
    public List<String> complete(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}

