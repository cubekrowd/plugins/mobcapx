package net.cubekrowd.mobcapx.command;

import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import net.cubekrowd.mobcapx.MobCapXPlugin;
import org.bukkit.command.CommandSender;

public final class AboutCommand implements MobCapXCommand.Subcommand {
    private final MobCapXPlugin plugin;

    public AboutCommand(MobCapXPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public void execute(CommandSender sender, String[] args, Message.Builder m) {
        var version = plugin.getDescription().getVersion();
        var website = plugin.getDescription().getWebsite();
        var authorsJoiner = new StringJoiner(", ");
        plugin.getDescription().getAuthors().forEach(authorsJoiner::add);

        var msg = m.info().text("About the plugin:");
        msg.bullet().text("Author(s): ").highlight(authorsJoiner.toString());
        msg.bullet().text("Version: ").highlight(version);
        msg.bullet().text("Website: ").highlight(website).openUrl(website);
        msg.send(sender);
    }

    @Override
    public List<String> complete(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}

