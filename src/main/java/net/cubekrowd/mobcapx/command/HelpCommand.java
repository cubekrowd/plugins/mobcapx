package net.cubekrowd.mobcapx.command;

import java.util.Collections;
import java.util.List;
import net.cubekrowd.mobcapx.MobCapXPlugin;
import org.bukkit.command.CommandSender;

public final class HelpCommand implements MobCapXCommand.Subcommand {
    private final MobCapXPlugin plugin;

    public HelpCommand(MobCapXPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public void execute(CommandSender sender, String[] args, Message.Builder m) {
        var msg = m.info().text("Accessible subcommands:");
        boolean empty = true;

        for (var sub : plugin.getMainCommand().getSubcommands()) {
            if (sub.hasPermission(sender)) {
                empty = false;
                msg.bullet().highlight(sub.getName() + " " + sub.getUsage());
            }
        }

        if (empty) {
            msg.text(" none.");
        }

        msg.send(sender);
    }

    @Override
    public List<String> complete(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}

