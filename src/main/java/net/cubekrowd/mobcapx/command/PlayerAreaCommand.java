package net.cubekrowd.mobcapx.command;

import java.util.Collections;
import java.util.List;
import net.cubekrowd.mobcapx.MobCapXPlugin;
import net.cubekrowd.mobcapx.MobCategory;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class PlayerAreaCommand implements PlayerCommand.Subcommand {
    private final MobCapXPlugin plugin;

    public PlayerAreaCommand(MobCapXPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getName() {
        return "area";
    }

    @Override
    public String getPermission() {
        return "mobcapx.debug";
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public void execute(CommandSender sender, Player target, String[] args, Message.Builder m) {
        var msg = m.info().text("Area info for ").highlight(target.getName()).text(":");
        var targetData = plugin.getPlayerData(target.getUniqueId());
        int serverTick = plugin.getServer().getCurrentTick();

        for (var category : MobCategory.values()) {
            if (category == MobCategory.MISC) {
                continue;
            }

            float areaValue = targetData.getAreaValue(category);
            float areaLimit = targetData.getAreaLimit(category);

            msg.bullet().text(category + " value: ").highlight(areaValue + " / " + areaLimit);
        }

        for (var world : plugin.getServer().getWorlds()) {
            int nearby = 0;
            int stale = 0;

            for (var loadedChunk : world.getLoadedChunks()) {
                var chunkData = plugin.getChunkMap().getChunkData(loadedChunk);

                if (chunkData.isNearby(target)) {
                    nearby++;

                    if (chunkData.isStale(plugin, serverTick)) {
                        stale++;
                    }
                }
            }

            if (nearby == 0) {
                // don't display world in list if no chunks there
                continue;
            }

            msg.bullet().text(world.getName() + " chunks: ")
                    .highlight(nearby + " (" + stale + " stale)");
        }

        msg.send(sender);
    }

    @Override
    public List<String> complete(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}
