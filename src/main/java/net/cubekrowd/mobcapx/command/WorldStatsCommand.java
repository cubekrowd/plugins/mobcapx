package net.cubekrowd.mobcapx.command;

import java.util.Collections;
import java.util.List;
import net.cubekrowd.mobcapx.MobCapXPlugin;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

public final class WorldStatsCommand implements WorldCommand.Subcommand {
    private final MobCapXPlugin plugin;

    public WorldStatsCommand(MobCapXPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getName() {
        return "stats";
    }

    @Override
    public String getPermission() {
        return "mobcapx.debug";
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public void execute(CommandSender sender, World world, String[] args, Message.Builder m) {
        var msg = m.info().text("Statistics for world ").highlight(world.getName()).text(":");
        var loadedChunks = world.getLoadedChunks();
        int staleCount = 0;
        int serverTick = plugin.getServer().getCurrentTick();

        for (var loadedChunk : loadedChunks) {
            var chunkData = plugin.getChunkMap().getChunkData(loadedChunk);

            if (chunkData.isStale(plugin, serverTick)) {
                staleCount++;
            }
        }

        int chunkDataCount = plugin.getChunkMap().size(world);

        msg.bullet().text("Loaded chunks: ").highlight(String.valueOf(loadedChunks.length));
        msg.bullet().text("Chunk data: ").highlight(String.valueOf(chunkDataCount));
        msg.bullet().text("Stale chunks: ").highlight(String.valueOf(staleCount));
        msg.bullet().text("Stale age: ").highlight(String.valueOf(plugin.getStaleChunkAge()));

        msg.send(sender);
    }

    @Override
    public List<String> complete(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}
