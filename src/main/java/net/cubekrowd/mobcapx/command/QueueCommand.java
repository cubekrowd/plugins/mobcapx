package net.cubekrowd.mobcapx.command;

import java.util.Collections;
import java.util.List;
import net.cubekrowd.mobcapx.MobCapXPlugin;
import org.bukkit.command.CommandSender;

public final class QueueCommand implements MobCapXCommand.Subcommand {
    private final MobCapXPlugin plugin;

    public QueueCommand(MobCapXPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getName() {
        return "queue";
    }

    @Override
    public String getPermission() {
        return "mobcapx.debug";
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public void execute(CommandSender sender, String[] args, Message.Builder m) {
        var msg = m.info().text("Current chunk task queue:");

        var tasks = plugin.getQueue().getTasks();

        for (int i = 0; i < tasks.size(); i++) {
            var taskName = tasks.get(i).getName();
            int j;

            for (j = i + 1; j < tasks.size(); j++) {
                if (!tasks.get(j).getName().equals(taskName)) {
                    break;
                }
            }

            int repeats = j - i;
            msg.bullet().text(taskName + " ").highlight("x" + repeats);
            i = j - 1;
        }

        if (tasks.isEmpty()) {
            msg.text(" empty.");
        }

        msg.send(sender);
    }

    @Override
    public List<String> complete(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}
