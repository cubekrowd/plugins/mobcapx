package net.cubekrowd.mobcapx.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

public final class Message {
    private final ComponentBuilder builder;
    private final ChatColor base;
    private final ChatColor highlight;

    public Message(Plugin plugin, ChatColor base, ChatColor highlight) {
        builder = new ComponentBuilder("[").color(ChatColor.GRAY)
                .append(plugin.getName()).color(ChatColor.DARK_PURPLE)
                .append("] ").color(ChatColor.GRAY);
        this.base = base;
        this.highlight = highlight;
    }

    public Message text(String text) {
        builder.append(text).color(base);
        return this;
    }

    public Message highlight(String text) {
        builder.append(text).color(highlight);
        return this;
    }

    public Message bullet() {
        builder.append("\n- ").color(ChatColor.GRAY);
        return this;
    }

    public Message openUrl(String url) {
        builder.event(new ClickEvent(ClickEvent.Action.OPEN_URL, url));
        return this;
    }

    public void send(CommandSender receiver) {
        receiver.spigot().sendMessage(builder.create());
    }

    public static final class Builder {
        private final Plugin plugin;

        public Builder(Plugin plugin) {
            this.plugin = plugin;
        }

        public Message info() {
            return new Message(plugin, ChatColor.GOLD, ChatColor.LIGHT_PURPLE);
        }

        public Message warn() {
            return new Message(plugin, ChatColor.RED, ChatColor.DARK_RED);
        }
    }
}
