package net.cubekrowd.mobcapx;

public final class MobCountTask implements ChunkTask {
    private final ChunkData chunkData;

    public MobCountTask(ChunkData chunkData) {
        this.chunkData = chunkData;
    }

    @Override
    public String getName() {
        return "Count chunk mobs";
    }

    @Override
    public int run(ChunkTaskQueue queue) {
        var plugin = queue.getPlugin();
        var chunk = chunkData.getChunk();

        if (chunk == null) {
            // chunk got unloaded, so end the task
            return 1;
        }

        int nearbyPlayerCount = chunkData.nearbyPlayerCount();
        float[] categoryCounts = new float[MobCategory.ENUM_COUNT - 1];

        int res = 0;

        // according to profiling results, this is a very expensive call, so we
        // add great weight per entity below
        var entities = chunk.getEntities();

        for (var entity : entities) {
            if (!entity.isValid()) {
                // Currently (MC 1.15.1) there's a bug in Spigot/Paper which
                // causes entities to appear in chunks even though they are not
                // actually there... By ignoring invalid entities here, we
                // circumvent this issue.
                continue;
            }

            var type = MobType.fromEntityType(entity.getType());
            if (type == null) {
                // not a mob
                continue;
            }

            var category = type.getCategory();
            if (category == MobCategory.MISC) {
                // we don't limit/track MISC entities
                continue;
            }

            // @TODO Vanilla Minecraft doesn't count mobs that satisfy the
            // following check:
            //
            //   var4.isPersistenceRequired()
            //       || var4.requiresCustomPersistence()
            //
            // See ServerLevel#getCategoryCounts. We could try to invoke those
            // two methods directly, but I'm getting the impression from some
            // Paper GitHub posts that Spigot modifies persistence behaviour, so
            // maybe we should just try to emulate them instead.
            //
            // Currently we're not handling mobs that hold items correctly, and
            // other mobs such as witch hut cats (which have persistence
            // required = true in vanilla).
            //
            // Note that entities with name tags indeed have persistence
            // required set to true in vanilla.
            if (entity.getCustomName() != null) {
                continue;
            }

            categoryCounts[category.ordinal()] += 1;
        }

        res += 4 + 10 * entities.length;

        // now update nearby player area values

        chunkData.updateWeight(plugin);
        float weight = chunkData.getWeight();

        var nearbyPlayerIter = chunkData.nearbyPlayerIterator();

        while (nearbyPlayerIter.hasNext()) {
            var nearbyPlayerId = nearbyPlayerIter.next();
            var playerData = plugin.getPlayerData(nearbyPlayerId);
            playerData.incrementAreaValues(chunkData.getCategoryCounts(), -weight);
            playerData.incrementAreaValues(categoryCounts, weight);
        }

        // update chunk's category counts
        chunkData.setCategoryCounts(categoryCounts, plugin.getServer().getCurrentTick());

        queue.add(this);
        return res + 2 * nearbyPlayerCount;
    }
}
