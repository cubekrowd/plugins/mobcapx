package net.cubekrowd.mobcapx;

public enum  MobCategory {
    MONSTER("monsters", "Monsters"),
    ANIMAL("animals", "Animals"),
    WATER_ANIMAL("water-animals", "Water animals"),
    AMBIENT("ambient", "Ambient"),
    MISC(null, "Misc");

    public static final int ENUM_COUNT = MobCategory.values().length;

    private String configString;
    private String string;

    MobCategory(String configString, String string) {
        this.configString = configString;
        this.string = string;
    }

    public String getConfigString() {
        return configString;
    }

    @Override
    public String toString() {
        return string;
    }
}
