package net.cubekrowd.mobcapx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.Chunk;
import org.bukkit.World;

public final class ChunkMap {
    private final List<WorldChunkMap> chunkMaps = new ArrayList<>();
    // mutable chunk ID for reuse to prevent GC overload
    private final ChunkId mutableChunkId = ChunkId.mutableOf(0, 0);

    private static final class WorldChunkMap {
        private final Map<ChunkId, ChunkData> map = new HashMap<>();
        private final World world;

        public WorldChunkMap(World world) {
            this.world = world;
        }
    }

    public ChunkData addChunkIfAbsent(Chunk chunk) {
        var id = ChunkId.of(chunk);
        var chunkData = new ChunkData(chunk);

        for (var chunkMap : chunkMaps) {
            if (chunkMap.world.equals(chunk.getWorld())) {
                var current = chunkMap.map.putIfAbsent(id, chunkData);
                // return null if chunk already present
                return current != null ? null : chunkData;
            }
        }

        // world not added yet, so create it
        var chunkMap = new WorldChunkMap(chunk.getWorld());
        chunkMaps.add(chunkMap);

        chunkMap.map.put(id, chunkData);
        return chunkData;
    }

    public ChunkData removeChunk(Chunk chunk) {
        mutableChunkId.setChunk(chunk);
        return removeChunk(chunk.getWorld(), mutableChunkId);
    }

    public ChunkData removeChunk(World world, ChunkId chunkId) {
        for (var chunkMap : chunkMaps) {
            if (chunkMap.world.equals(world)) {
                return chunkMap.map.remove(chunkId);
                // @TODO remove chunk map if internal map is empty?
            }
        }
        return null;
    }

    public ChunkData getChunkData(Chunk chunk) {
        mutableChunkId.setChunk(chunk);

        for (var chunkMap : chunkMaps) {
            if (chunkMap.world.equals(chunk.getWorld())) {
                return chunkMap.map.get(mutableChunkId);
            }
        }
        return null;
    }

    public ChunkData getChunkData(World world, ChunkId chunkId) {
        for (var chunkMap : chunkMaps) {
            if (chunkMap.world.equals(world)) {
                return chunkMap.map.get(chunkId);
            }
        }
        return null;
    }

    public void clear() {
        chunkMaps.clear();
    }

    public int size(World world) {
        for (var chunkMap : chunkMaps) {
            if (chunkMap.world.equals(world)) {
                return chunkMap.map.size();
            }
        }
        return 0;
    }
}
