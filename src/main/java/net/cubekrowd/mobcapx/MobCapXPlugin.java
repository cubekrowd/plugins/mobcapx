package net.cubekrowd.mobcapx;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import net.cubekrowd.mobcapx.command.MobCapXCommand;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public final class MobCapXPlugin extends JavaPlugin implements Listener {
    private final ChunkMap chunkMap = new ChunkMap();
    private final Map<UUID, PlayerData> playerMap = new HashMap<>();
    private ChunkTaskQueue queue;
    private MobCapXCommand mainCommand;
    private int nearbyRange;
    // limits for all except MISC
    private float[] nearbyLimits = new float[MobCategory.ENUM_COUNT - 1];
    private float chunkWeightFactor;
    private int chunkOperationsPerTick;
    private int staleChunkAge;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        reloadConfig();

        queue = new ChunkTaskQueue(this);

        mainCommand = new MobCapXCommand(this);
        getCommand("mobcapx").setExecutor(mainCommand);

        getServer().getPluginManager().registerEvents(new EventListener(this), this);

        for (var world : getServer().getWorlds()) {
            for (var loadedChunk : world.getLoadedChunks()) {
                registerChunk(loadedChunk);
            }
        }

        for (var onlinePlayer : getServer().getOnlinePlayers()) {
            registerPlayer(onlinePlayer);
        }

        getServer().getScheduler().runTaskTimer(this, () -> queue.tick(), 1, 1);
    }

    @Override
    public void onDisable() {
        mainCommand = null;
        chunkMap.clear();
        playerMap.clear();
        queue = null;
    }

    // It's completely fine to override this. The Bukkit API will handle this
    // internally correctly as well.
    @Override
    public void reloadConfig() {
        super.reloadConfig();

        // update our cached variables so we don't need to go through the
        // Configuration API during hot loops and such

        assert MobCategory.MISC.ordinal() == MobCategory.ENUM_COUNT - 1;
        nearbyRange = getConfig().getInt("nearby-range");

        for (var category : MobCategory.values()) {
            if (category == MobCategory.MISC) {
                continue;
            }

            var key = category.getConfigString();
            nearbyLimits[category.ordinal()] = getConfig().getInt("nearby-limits." + key);
        }

        chunkWeightFactor = (float) getConfig().getDouble("chunk-weight-factor");
        chunkOperationsPerTick = getConfig().getInt("chunk-operations-per-tick");
        staleChunkAge = getConfig().getInt("stale-chunk-age");
    }

    public MobCapXCommand getMainCommand() {
        return mainCommand;
    }

    public int getNearbyRange() {
        return nearbyRange;
    }

    public float computeLimit(MobCategory category, int nearbyChunks) {
        // category-limit * nearby-chunk-count / 289
        return nearbyLimits[category.ordinal()] * nearbyChunks / 289f;
    }

    public float computeWeight(int nearbyPlayerCount) {
        if (nearbyPlayerCount == 0) {
            return 0;
        }
        // 1 / log(e + chunk-weight-factor * (nearby-player-count - 1))
        // @TODO do float math here instead of double math?
        return (float) (1 / Math.log(Math.E + chunkWeightFactor * (nearbyPlayerCount - 1)));
    }

    public void registerChunk(Chunk chunk) {
        var chunkData = chunkMap.addChunkIfAbsent(chunk);
        if (chunkData != null) {
            queue.add(new MobCountTask(chunkData));
        }
    }

    public void deregisterChunk(Chunk chunk) {
        var chunkData = chunkMap.removeChunk(chunk);
        if (chunkData != null) {
            chunkData.unload();
        }
    }

    public ChunkMap getChunkMap() {
        return chunkMap;
    }

    public PlayerData getPlayerData(UUID playerId) {
        return playerMap.get(playerId);
    }

    public void registerPlayer(Player player) {
        if (!playerMap.containsKey(player.getUniqueId())) {
            // assumes player is currently online
            playerMap.put(player.getUniqueId(), new PlayerData());
            getQueue().add(new UpdatePlayerTask(player));
        }
    }

    public void removePlayerData(UUID playerId) {
        playerMap.remove(playerId);
    }

    public ChunkTaskQueue getQueue() {
        return queue;
    }

    public int getChunkOperationsPerTick() {
        return chunkOperationsPerTick;
    }

    public int getStaleChunkAge() {
        return staleChunkAge;
    }
}
