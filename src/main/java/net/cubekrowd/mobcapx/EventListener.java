package net.cubekrowd.mobcapx;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

public final class EventListener implements Listener {
    private final MobCapXPlugin plugin;

    public EventListener(MobCapXPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent e) {
        plugin.registerChunk(e.getChunk());
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent e) {
        plugin.deregisterChunk(e.getChunk());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        // The player update task removes the player data when it detects
        // the player is offline. If the player relogs before that happens,
        // we just reuse the old player update task. Otherwise, we create a new
        // player update task and create the player data.
        var playerData = plugin.getPlayerData(e.getPlayer().getUniqueId());
        if (playerData == null) {
            plugin.registerPlayer(e.getPlayer());
        } else {
            playerData.setOnline(true);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        plugin.getPlayerData(e.getPlayer().getUniqueId()).setOnline(false);
    }

    @EventHandler(ignoreCancelled = true)
    public void onCreatureSpawn(CreatureSpawnEvent e) {
        if (e.getSpawnReason() != CreatureSpawnEvent.SpawnReason.NATURAL) {
            return;
        }

        var spawnType = MobType.fromEntityType(e.getEntityType());

        if (spawnType == null) {
            // not a mob???
            return;
        }

        var spawnCategory = spawnType.getCategory();

        if (spawnCategory == MobCategory.MISC) {
            // we don't limit MISC mobs
            return;
        }

        var spawnChunk = e.getLocation().getChunk();
        var spawnChunkData = plugin.getChunkMap().getChunkData(spawnChunk);
        int serverTick = plugin.getServer().getCurrentTick();

        if (spawnChunkData.isStale(plugin, serverTick)) {
            // can't spawn mobs in stale chunks
            e.setCancelled(true);
            return;
        }

        int spawnNearbyPlayers = spawnChunkData.nearbyPlayerCount();

        if (spawnNearbyPlayers == 0) {
            // if no players nearby, then spawning not allowed
            e.setCancelled(true);
            return;
        }

        float spawnValue = spawnChunkData.getWeight();
        var nearbyPlayerIter = spawnChunkData.nearbyPlayerIterator();

        // cache player datas so we can iterate over them very quickly below
        var playerDatas = new PlayerData[spawnNearbyPlayers];
        int index = 0;

        while (nearbyPlayerIter.hasNext()) {
            var nearbyPlayerId = nearbyPlayerIter.next();
            var playerData = plugin.getPlayerData(nearbyPlayerId);
            playerDatas[index] = playerData;
            index++;

            if (!playerData.isOnline()) {
                continue;
            }

            float areaValue = playerData.getAreaValue(spawnCategory);
            float areaLimit = playerData.getAreaLimit(spawnCategory);

            if (areaValue + spawnValue > areaLimit) {
                e.setCancelled(true);
                return;
            }
        }

        // update chunk's total weight and player area values if spawn is
        // allowed

        // If some other plugin decides to cancel the event anyway later on,
        // this results in an area value that is too high, but this will
        // probably be fixed within a few ticks by the mob count task.
        spawnChunkData.incrementCategoryCount(spawnCategory, 1);
        for (PlayerData playerData : playerDatas) {
            playerData.incrementAreaValue(spawnCategory, spawnValue);
        }
    }
}
