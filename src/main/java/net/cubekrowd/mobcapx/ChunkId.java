package net.cubekrowd.mobcapx;

import org.bukkit.Chunk;

// Chunk IDs are a per-world thing, they don't contain world information. This
// is mainly because the mob cap is handled on a per-world basis and therefore
// chunk information is also stored per-world.
public abstract class ChunkId {
    private ChunkId() {}

    public static ChunkId of(int x, int z) {
        return new ImmutableChunkId(x, z);
    }

    public static ChunkId of(Chunk chunk) {
        return of(chunk.getX(), chunk.getZ());
    }

    public static ChunkId mutableOf(int x, int z) {
        return new MutableChunkId(x, z);
    }

    public static ChunkId mutableOf(Chunk chunk) {
        return mutableOf(chunk.getX(), chunk.getZ());
    }

    public abstract int getX();

    public abstract int getZ();

    public abstract void setX(int newX);

    public abstract void setZ(int newZ);

    public abstract void setXZ(int newX, int newZ);

    public void setChunk(Chunk newChunk) {
        setXZ(newChunk.getX(), newChunk.getZ());
    }

    public boolean isChunk(Chunk compare) {
        return compare.getX() == getX() && compare.getZ() == getZ();
    }

    @Override
    public int hashCode() {
        return (getX() << 31) + getZ();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ChunkId)) {
            return false;
        }

        var o = (ChunkId) obj;
        return o.getX() == getX() && o.getZ() == getZ();
    }

    private static final class ImmutableChunkId extends ChunkId {
        private final int x;
        private final int z;

        private ImmutableChunkId(int x, int z) {
            this.x = x;
            this.z = z;
        }

        @Override
        public int getX() {
            return x;
        }

        @Override
        public int getZ() {
            return z;
        }

        @Override
        public void setX(int newX) {
            throw new UnsupportedOperationException("Immutable chunk ID");
        }

        @Override
        public void setZ(int newZ) {
            throw new UnsupportedOperationException("Immutable chunk ID");
        }

        @Override
        public void setXZ(int newX, int newZ) {
            throw new UnsupportedOperationException("Immutable chunk ID");
        }
    }

    private static final class MutableChunkId extends ChunkId {
        private int x;
        private int z;

        private MutableChunkId(int x, int z) {
            this.x = x;
            this.z = z;
        }

        @Override
        public int getX() {
            return x;
        }

        @Override
        public int getZ() {
            return z;
        }

        @Override
        public void setX(int newX) {
            x = newX;
        }

        @Override
        public void setZ(int newZ) {
            z = newZ;
        }

        @Override
        public void setXZ(int newX, int newZ) {
            x = newX;
            z = newZ;
        }
    }
}
