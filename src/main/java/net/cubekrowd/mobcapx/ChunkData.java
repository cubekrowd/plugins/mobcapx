package net.cubekrowd.mobcapx;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.UUID;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;

public final class ChunkData {
    private static final float GROWTH_FACTOR = 1.5f;
    private static final UUID[] NO_PLAYERS = new UUID[0];
    private UUID[] nearbyPlayers = NO_PLAYERS;
    private short playerCount;
    // Keep reference to chunk for fast lookups, gets removed when the chunk has
    // unloaded by the event listener. Chunk data inside the chunk map always
    // have non-null chunks.
    private Chunk chunk;
    // don't track MISC category
    private float[] categoryCounts = new float[MobCategory.ENUM_COUNT - 1];
    // Every mob in this chunk should be weighted according to this weight. If
    // this value is changed, all area values of nearby players should be
    // updated accordingly!
    private float weight;
    // Server tick in which the category counts were last recalculated fully
    // using the mob count task. If this is too long ago, natural spawning will
    // be cancelled in the chunk.
    // -1 means mobs have never been counted for this chunk
    private int recalculateCategoryCountTick = -1;

    public ChunkData(Chunk chunk) {
        this.chunk = chunk;
    }

    public Chunk getChunk() {
        return chunk;
    }

    public void unload() {
        chunk = null;
    }

    public void addNearbyPlayer(Player newPlayer) {
        if (playerCount == nearbyPlayers.length) {
            // array is full, so grow it
            int oldLength = nearbyPlayers.length;
            // add one to guarantee growth
            nearbyPlayers = Arrays.copyOf(nearbyPlayers, (int) (oldLength * GROWTH_FACTOR) + 1);
            nearbyPlayers[oldLength] = newPlayer.getUniqueId();
            playerCount++;
            return;
        }

        // array is not full, so find an empty spot
        for (int i = 0; i < nearbyPlayers.length; i++) {
            if (nearbyPlayers[i] == null) {
                nearbyPlayers[i] = newPlayer.getUniqueId();
                playerCount++;
                return;
            }
        }
    }

    public void removeNearbyPlayer(UUID playerId) {
        for (int i = 0; i < nearbyPlayers.length; i++) {
            // Note: nearbyPlayers[i] may be null, so order of comparison is
            // important here to avoid null pointers!
            if (playerId.equals(nearbyPlayers[i])) {
                nearbyPlayers[i] = null;
                playerCount--;
                // @TODO decrease array capacity when suitable
                return;
            }
        }
    }

    public float[] getCategoryCounts() {
        return categoryCounts;
    }

    public void setCategoryCounts(float[] newCategoryCounts, int serverTick) {
        categoryCounts = newCategoryCounts;
        recalculateCategoryCountTick = serverTick;
    }

    public boolean isStale(MobCapXPlugin plugin, int serverTick) {
        if (recalculateCategoryCountTick == -1) {
            return true;
        } else {
            return serverTick - recalculateCategoryCountTick >= plugin.getStaleChunkAge();
        }
    }

    public int getRecalculateCategoryCountTick() {
        return recalculateCategoryCountTick;
    }

    public void incrementCategoryCount(MobCategory category, float add) {
        categoryCounts[category.ordinal()] += add;
    }

    public void updateWeight(MobCapXPlugin plugin) {
        weight = plugin.computeWeight(playerCount);
    }

    public float getWeight() {
        return weight;
    }

    public short nearbyPlayerCount() {
        return playerCount;
    }

    public Iterator<UUID> nearbyPlayerIterator() {
        return new NearbyPlayerIterator();
    }

    public boolean isNearby(Player target) {
        var nearbyPlayerIter = nearbyPlayerIterator();

        while (nearbyPlayerIter.hasNext()) {
            var nearbyPlayerId = nearbyPlayerIter.next();
            if (nearbyPlayerId.equals(target.getUniqueId())) {
                return true;
            }
        }
        return false;
    }

    private final class NearbyPlayerIterator implements Iterator<UUID> {
        private int index;
        private int visited;

        @Override
        public boolean hasNext() {
            return visited < playerCount;
        }

        @Override
        public UUID next() {
            for (;;) {
                if (visited == playerCount) {
                    throw new NoSuchElementException();
                }

                var res = nearbyPlayers[index];
                index++;

                if (res != null) {
                    visited++;
                    return res;
                }
            }
        }
    }
}
