package net.cubekrowd.mobcapx;

public interface ChunkTask {
    String getName();

    int run(ChunkTaskQueue queue);
}
