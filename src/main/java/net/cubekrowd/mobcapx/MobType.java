package net.cubekrowd.mobcapx;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Mob;

public enum  MobType {
    BAT(EntityType.BAT, MobCategory.AMBIENT),
    BEE(EntityType.BEE, MobCategory.ANIMAL),
    BLAZE(EntityType.BLAZE, MobCategory.MONSTER),
    CAT(EntityType.CAT, MobCategory.ANIMAL),
    CAVE_SPIDER(EntityType.CAVE_SPIDER, MobCategory.MONSTER),
    CHICKEN(EntityType.CHICKEN, MobCategory.ANIMAL),
    COD(EntityType.COD, MobCategory.WATER_ANIMAL),
    COW(EntityType.COW, MobCategory.ANIMAL),
    CREEPER(EntityType.CREEPER, MobCategory.MONSTER),
    DONKEY(EntityType.DONKEY, MobCategory.ANIMAL),
    DOLPHIN(EntityType.DOLPHIN, MobCategory.WATER_ANIMAL),
    DROWNED(EntityType.DROWNED, MobCategory.MONSTER),
    ELDER_GUARDIAN(EntityType.ELDER_GUARDIAN, MobCategory.MONSTER),
    ENDER_DRAGON(EntityType.ENDER_DRAGON, MobCategory.MONSTER),
    ENDERMAN(EntityType.ENDERMAN, MobCategory.MONSTER),
    ENDERMITE(EntityType.ENDERMITE, MobCategory.MONSTER),
    EVOKER(EntityType.EVOKER, MobCategory.MONSTER),
    FOX(EntityType.FOX, MobCategory.ANIMAL),
    GHAST(EntityType.GHAST, MobCategory.MONSTER),
    GUARDIAN(EntityType.GUARDIAN, MobCategory.MONSTER),
    HORSE(EntityType.HORSE, MobCategory.ANIMAL),
    HUSK(EntityType.HUSK, MobCategory.MONSTER),
    ILLUSIONER(EntityType.ILLUSIONER, MobCategory.MONSTER),
    IRON_GOLEM(EntityType.IRON_GOLEM, MobCategory.MISC),
    LLAMA(EntityType.LLAMA, MobCategory.ANIMAL),
    MAGMA_CUBE(EntityType.MAGMA_CUBE, MobCategory.MONSTER),
    MULE(EntityType.MULE, MobCategory.ANIMAL),
    MUSHROOM_COW(EntityType.MUSHROOM_COW, MobCategory.ANIMAL),
    OCELOT(EntityType.OCELOT, MobCategory.ANIMAL),
    PANDA(EntityType.PANDA, MobCategory.ANIMAL),
    PARROT(EntityType.PARROT, MobCategory.ANIMAL),
    PHANTOM(EntityType.PHANTOM, MobCategory.MONSTER),
    PIG(EntityType.PIG, MobCategory.ANIMAL),
    PIG_ZOMBIE(EntityType.PIG_ZOMBIE, MobCategory.MONSTER),
    PILLAGER(EntityType.PILLAGER, MobCategory.MONSTER),
    POLAR_BEAR(EntityType.POLAR_BEAR, MobCategory.ANIMAL),
    PUFFERFISH(EntityType.PUFFERFISH, MobCategory.WATER_ANIMAL),
    RABBIT(EntityType.RABBIT, MobCategory.ANIMAL),
    RAVAGER(EntityType.RAVAGER, MobCategory.MONSTER),
    SALMON(EntityType.SALMON, MobCategory.WATER_ANIMAL),
    SHEEP(EntityType.SHEEP, MobCategory.ANIMAL),
    SHULKER(EntityType.SHULKER, MobCategory.MONSTER),
    SILVERFISH(EntityType.SILVERFISH, MobCategory.MONSTER),
    SNOWMAN(EntityType.SNOWMAN, MobCategory.MISC),
    SKELETON(EntityType.SKELETON, MobCategory.MONSTER),
    SKELETON_HORSE(EntityType.SKELETON_HORSE, MobCategory.ANIMAL),
    SLIME(EntityType.SLIME, MobCategory.MONSTER),
    SPIDER(EntityType.SPIDER, MobCategory.MONSTER),
    SQUID(EntityType.SQUID, MobCategory.WATER_ANIMAL),
    STRAY(EntityType.STRAY, MobCategory.MONSTER),
    TRADER_LLAMA(EntityType.TRADER_LLAMA, MobCategory.ANIMAL),
    TROPICAL_FISH(EntityType.TROPICAL_FISH, MobCategory.WATER_ANIMAL),
    TURTLE(EntityType.TURTLE, MobCategory.ANIMAL),
    VEX(EntityType.VEX, MobCategory.MONSTER),
    VILLAGER(EntityType.VILLAGER, MobCategory.MISC),
    VINDICATOR(EntityType.VINDICATOR, MobCategory.MONSTER),
    WANDERING_TRADER(EntityType.WANDERING_TRADER, MobCategory.ANIMAL),
    WITCH(EntityType.WITCH, MobCategory.MONSTER),
    WITHER(EntityType.WITHER, MobCategory.MONSTER),
    WITHER_SKELETON(EntityType.WITHER_SKELETON, MobCategory.MONSTER),
    WOLF(EntityType.WOLF, MobCategory.ANIMAL),
    ZOMBIE(EntityType.ZOMBIE, MobCategory.MONSTER),
    ZOMBIE_HORSE(EntityType.ZOMBIE_HORSE, MobCategory.ANIMAL),
    ZOMBIE_VILLAGER(EntityType.ZOMBIE_VILLAGER, MobCategory.MONSTER),
    UNKNOWN(null, MobCategory.MISC); // when server is running out-dated plugin

    public static final int ENUM_COUNT = MobType.values().length;
    private final EntityType entityType;
    private final MobCategory category;

    MobType(EntityType entityType, MobCategory category) {
        this.entityType = entityType;
        this.category = category;
    }

    public EntityType toEntityType() {
        return entityType;
    }

    public MobCategory getCategory() {
        return category;
    }

    public String getKey() {
        if (this == UNKNOWN) {
            return "unknown";
        } else {
            return toEntityType().getKey().getKey();
        }
    }

    public static MobType fromEntityType(EntityType entityType) {
        switch (entityType) {
        case BAT: return BAT;
        case BEE: return BEE;
        case BLAZE: return BLAZE;
        case CAT: return CAT;
        case CAVE_SPIDER: return CAVE_SPIDER;
        case CHICKEN: return CHICKEN;
        case COD: return COD;
        case COW: return COW;
        case CREEPER: return CREEPER;
        case DONKEY: return DONKEY;
        case DOLPHIN: return DOLPHIN;
        case DROWNED: return DROWNED;
        case ELDER_GUARDIAN: return ELDER_GUARDIAN;
        case ENDER_DRAGON: return ENDER_DRAGON;
        case ENDERMAN: return ENDERMAN;
        case ENDERMITE: return ENDERMITE;
        case EVOKER: return EVOKER;
        case FOX: return FOX;
        case GHAST: return GHAST;
        case GUARDIAN: return GUARDIAN;
        case HORSE: return HORSE;
        case HUSK: return HUSK;
        case ILLUSIONER: return ILLUSIONER;
        case IRON_GOLEM: return IRON_GOLEM;
        case LLAMA: return LLAMA;
        case MAGMA_CUBE: return MAGMA_CUBE;
        case MULE: return MULE;
        case MUSHROOM_COW: return MUSHROOM_COW;
        case OCELOT: return OCELOT;
        case PANDA: return PANDA;
        case PARROT: return PARROT;
        case PHANTOM: return PHANTOM;
        case PIG: return PIG;
        case PIG_ZOMBIE: return PIG_ZOMBIE;
        case PILLAGER: return PILLAGER;
        case POLAR_BEAR: return POLAR_BEAR;
        case PUFFERFISH: return PUFFERFISH;
        case RABBIT: return RABBIT;
        case RAVAGER: return RAVAGER;
        case SALMON: return SALMON;
        case SHEEP: return SHEEP;
        case SHULKER: return SHULKER;
        case SILVERFISH: return SILVERFISH;
        case SNOWMAN: return SNOWMAN;
        case SKELETON: return SKELETON;
        case SKELETON_HORSE: return SKELETON_HORSE;
        case SLIME: return SLIME;
        case SPIDER: return SPIDER;
        case SQUID: return SQUID;
        case STRAY: return STRAY;
        case TRADER_LLAMA: return TRADER_LLAMA;
        case TROPICAL_FISH: return TROPICAL_FISH;
        case TURTLE: return TURTLE;
        case VEX: return VEX;
        case VILLAGER: return VILLAGER;
        case VINDICATOR: return VINDICATOR;
        case WANDERING_TRADER: return WANDERING_TRADER;
        case WITCH: return WITCH;
        case WITHER: return WITHER;
        case WITHER_SKELETON: return WITHER_SKELETON;
        case WOLF: return WOLF;
        case ZOMBIE: return ZOMBIE;
        case ZOMBIE_HORSE: return ZOMBIE_HORSE;
        case ZOMBIE_VILLAGER: return ZOMBIE_VILLAGER;
        default:
            if (Mob.class.isAssignableFrom(entityType.getEntityClass())) {
                return UNKNOWN;
            } else {
                // not a mob
                return null;
            }
        }
    }
}
