# MobCapX

**Notice**: The problems this plugin attempts to solve are solved by Paper's per-player mob spawns in a much more robust way. Since it is built right into the server implementation, it ignores named mobs, witch hut cats, etc. without the need to update mob counting manually every Minecraft update, as needs to be done for this plugin.

This is a plugin that hopes to improve the SMP mob spawning algorithm by giving each player their own mob cap on top of the global vanilla mob cap. This should fix the insane vanilla guardian spawning (500 guardians is not unheard of on CubeKrowd) and the distribution of the mob cap.

Simply said, the plugin restricts mob spawns based on the number of players that are close to the spawn location. If there are no players nearby, spawning is cancelled. The more players there are in an area, the more mobs are allowed to spawn there. This contrasts with vanilla spawning, where spawns generally happen regardless of the player distribution.

## Technical overview

We first provide an overview of the vanilla mob cap. In vanilla Minecraft, there are different mob cabs for the categories monsters, animals, water animals and ambient mobs (currently only bats). These four mob caps depend on the number of chunks that are loaded and within a 17-by-17 area around any player. If we call this number of chunks $`x`$, then the mob cap for a specific category equals $`l \cdot x / 289`$ rounded down. In vanilla $`l`$ equals 70 for mobs, 10 for animals, 15 for water animals and 15 for ambient mobs, although this is configurable on servers.

Every tick, vanilla Minecraft's natural spawning code tries to spawn mobs in every loaded chunk, unless the mob cap described above, has been reached. There isn't anything that prevents mob flooding in specific areas (such as ocean monuments). Note that tons of mobs in one area means that less will spawn in other areas.

Our algorithm attempts to fix these issues.
The user can configure the algorithm with three principal ingredients:

* **nearby-range**: this defines how many chunks out should be counted towards a player's personal mob cap. For example, if this is set to 8, then all mobs within a 17-by-17 square around the player count towards their mob cap. We call this area the player's nearby area. Note that mobs only spawn inside nearby areas, i.e. only if there is a player nearby.
* **nearby-limits**: this defines a per-player mob cap for the mob categories monsters, animals, water-animals and ambient, much like vanilla. The maximum number of mobs in each category in a player's nearby area is calculated the same way as in vanilla: $`l \cdot x / 289`$, where $`l`$ is the category's nearby limit and $`x`$ is the number of loaded chunks in the nearby area. Note that this formula really desribes the maximum mob weight. The actual mob limit differs if more than one player is nearby; see the next point.
* **chunk-weight**: this defines the weight of mobs inside of a chunk, that counts towards the mob weight of nearby players. The formula is: $`1 / \log(e + f \cdot (p - 1))`$. Here $`\log`$ is the natural logarithm, $`e \approx 2.71828`$, $`w`$ is the configurable chunk weight factor, and $`p`$ is the number of nearby players.

Here is an example of how all of this works. Suppose the nearby range is 10, the nearby limit for monsters is 70, the chunk weight factor is 1 and there are 2 players whose nearby areas overlap perfectly and are fully loaded. Then the mob weight in this area is $`70 \cdot 21^2 / 289 \approx 106`$ and the mob limit is $`106 * \log(e + 1 * (2 - 1)) \approx 139`$ in this area.

For more information, see the configuration file.

It should be emphasised that this plugin only cancels natural spawns. Hence spawners will spawn mobs regardless of the mob cap and new chunks will be generated with lots of animals.

## Commands

This section describes all the available commands in this plugin.

---
#### /mobcapx about
**Permission**: none  
Displays some generic information about the plugin. This doesn't contain any data you may want to keep private, except perhaps the version number.

---
#### /mobcapx world \<world\> stats
**Permission**: mobcapx.debug  
Shows you some statistics about the world, such as the number of loaded chunks, the number of stale chunks and the stale chunk age set in the configuration file.

---
#### /mobcapx player \<player\> area
**Permission**: mobcapx.debug  
Displays some information about the nearby area of the player, such as the mob category values and limits (weights).

---
#### /mobcapx player \<player\> chunks
**Permission**: mobcapx.debug  
Displays information internal to the plugin, about the chunks close to a player. More specifically, this shows the monster, animal, water animal and ambient counts as well as the chunk ages. Note that only chunks with a non-zero mob count are displayed here to avoid spam.

---
#### /mobcapx queue
**Permission**: mobcapx.debug  
Displays the entries in the chunk task queue. This queue is used to restrict the amount of work done per tick and affects how quickly the reload command takes effect.

---
#### /mobcapx reload
**Permission**: mobcapx.reload  
Reloads the configuration file from disk. Any changes will be applied, although they may not take effect immediately. This allows you to reconfigure the plugin without reloading it
via some plugin manager and without restarting the server.

---
#### /mobcapx help
**Permission**: none  
Displays all subcommands you have access to.

## Permissions

Administrators can grand the following permissions to players. Note that players without the permission 'mobcapx.command' won't be able to 
use any command.

| Permission            | Description
|---                    |---
| mobcapx.command       | /mobcapx
| mobcapx.debug         | Debugging tools
| mobcapx.reload        | /mobcapx reload
